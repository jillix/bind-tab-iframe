module.exports = function (config) {
    var self = this;

    config.frameConfig.frame = config.frameConfig.frame || '#content-frame';
    config.frameConfig.defaultTab = config.frameConfig.defaultTab || '#tab1';
    config.frameConfig.css = config.frameConfig.css || [];
    config.frameConfig.tabs = config.frameConfig.tabs || {};
    self.config = config;

    var hash = window.location.hash;

    //get hash on load
    if (!hash || !config.frameConfig.tabs[hash]) {
        activateTab.call(self, config.frameConfig.tabs[config.frameConfig.defaultTab]);
    } else {
        activateTab.call(self, config.frameConfig.tabs[hash])
    }

    //hashchange handler
    $(window).on("hashchange", function () {
        var hash = window.location.hash;
        if (!hash || !config.frameConfig.tabs[hash]) {
            activateTab.call(self, config.frameConfig.tabs[config.frameConfig.defaultTab]);
        } else {
            activateTab.call(self, config.frameConfig.tabs[hash]);
        }
    });

}

function activateTab (html) {

    var self = this;

    if (typeof html === 'object') {
        html = html[M.getLocale()];
    }

    $(self.config.frameConfig.frame).get(0).contentWindow.location.replace(html);

    //handle selectors
    var path = window.location.pathname + window.location.hash;
    if (path.indexOf('/fr') != -1 || path.indexOf('/de') != -1 || path.indexOf('/it') != -1) {
        path = path = path.substring(3);
    }

    $(self.config.selectors.elementClass).removeClass("active");
    $(self.config.selectors.elementClass + "[href='" + path + "']").addClass("active");

    // remove first '/'
    path = path.substring(1);
    $(self.config.selectors.elementClass + "[href='" + path + "']").addClass("active");

    //resize the frame
    $(self.config.frameConfig.frame).load(function() {

        //apply css
        if (self.config.frameConfig.css.length) {
            var css = self.config.frameConfig.css;
            var doc = document.getElementById(self.config.frameConfig.frame.substr(1)).contentWindow.document;
            for (var i = 0; i < css.length; ++i) {
                var cssLink = doc.createElement("link");
                cssLink.href = css[i];
                cssLink.rel = "stylesheet";
                cssLink.type = "text/css";

                doc.head.appendChild(cssLink);
            }
        }

        //TODO
        $(self.config.frameConfig.frame).height($(self.config.frameConfig.frame).contents().find(".services-wrapper").height() + 30);

        //handle resize
        $(window).resize(function() {
            $(self.config.frameConfig.frame).height($(self.config.frameConfig.frame).contents().find(".services-wrapper").height() +30);
        });
    });
}
<<<<<<< HEAD
=======

>>>>>>> ccc5e19
